package com.booking.service;

import java.util.List;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class PrintService {
    public static void printMenu(String title, String[] menuArr){
        int num = 1;
        System.out.println(title);
        for (int i = 0; i < menuArr.length; i++) {
            if (i == (menuArr.length - 1)) {   
                num = 0;
            }
            System.out.println(num + ". " + menuArr[i]);   
            num++;
        }
    }

    public static String printServices(List<Service> serviceList){
        String result = "";
        // Bisa disesuaikan kembali
        for (Service service : serviceList) {
            result += service.getServiceName() + ", ";
        }
        return result;
    }

    // Function yang dibuat hanya sebgai contoh bisa disesuaikan kembali
    public static void showRecentReservation(List<Reservation> reservationList){
        int num = 1;
        System.out.printf("| %-4s | %-8s | %-11s | %-50s | %-15s | %-15s |\n",
                "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Workstage");
        System.out.println("+============================================================================================================+");
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Waiting") || reservation.getWorkstage().equalsIgnoreCase("In process")) {
                System.out.printf("| %-4s | %-8s | %-11s | %-50s | %-15s | %-15s |\n",
                num, reservation.getReservationId(), reservation.getCustomer().getName(), printServices(reservation.getServices()), reservation.getReservationPrice(), reservation.getWorkstage());
                num++;
            }
        }
    }

    public static void showAllCustomer(List<Person> listPersons){
        int [] num = {1};
        System.out.printf("| %-4s | %-10s | %-15s | %-15s | %-15s |\n",
                "No.", "ID", "Nama Customer", "Adress", "Membership");
        System.out.println("+==================================================================================================+");
        
        listPersons.stream()
                    .filter(person -> person instanceof Customer)
                    .map(person -> (Customer)person)
                    .forEach(person -> {
                        System.out.printf("| %-4s | %-10s | %-15s | %-15s | %-10s |\n",
                        num[0], person.getId(), person.getName(), person.getAddress(), person.getMember().getMembershipName() );
                        num[0]++;
                    });
    }

    public static void showAllEmployee(List<Person> listPersons){
        int[] num = {1};
        System.out.printf("| %-4s | %-10s | %-15s | %-15s | %-10s |\n",
                "No.", "ID", "Nama Employee", "Adress", "Experience");
        System.out.println("+========================================================================================+");
        listPersons.stream()
                    .filter(person -> person instanceof Employee)
                    .map(person -> (Employee)person)
                    .forEach(person -> {
                        System.out.printf("| %-4s | %-10s | %-15s | %-15s | %-10s |\n",
                        num[0], person.getId(), person.getName(), person.getAddress(), person.getExperience() );
                        num[0]++;
                    });
    }

    public static void showHistoryReservation(List<Reservation> reservationList){
        int num = 1;
        double totalPrice = 0;
        System.out.printf("| %-4s | %-8s | %-11s | %-60s | %-15s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Workstage");
        System.out.println("+======================================================================================================================================+");
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Finish") || reservation.getWorkstage().equalsIgnoreCase("Cancel"))  {
                System.out.printf("| %-4s | %-8s | %-11s | %-60s | %-15s | %-10s |\n",
                num, reservation.getReservationId(), reservation.getCustomer().getName(), printServices(reservation.getServices()), reservation.getReservationPrice(), reservation.getWorkstage());
            
                totalPrice += reservation.getReservationPrice();
                num++;
            }
        }
        System.out.printf("| %-92s | %-28s |\n",
                "Total Pemasukan",totalPrice);

    }
    public static void showAllService(List<Service> serviceList){
        int [] num = {1};
        System.out.printf("| %-4s | %-10s | %-25s | %-15s |\n",
                "No.", "ID", "Nama", "Harga");
        System.out.println("+========================================================================================+");
        
        serviceList.stream()
        .forEach(service -> {
            System.out.printf("| %-4s | %-10s | %-25s | %-15s |\n",
            num[0], service.getServiceId(), service.getServiceName(), service.getPrice() );
            num[0]++;
        });
    }


}
